using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Concrete Command [TurnOnCommand]
/// </summary>
/// <remarks>
/// <para> ConcreteCommand berfungsi untuk menginisiasi perintah spesifik untuk dijalankan pada receiver class. </para>
/// <para> TurnOnCommand berfungsi untuk menyalakan atau mematikan lampu</para>
/// </remarks>
public class TurnOnCommand : ICommand
{
    Lightbulb _lightbulb;

    public TurnOnCommand(Lightbulb lightbulb)
    {
        _lightbulb = lightbulb;
    }

    public void Execute()
    {
        _lightbulb.ToggleLight();
    }

    public void Undo()
    {
        _lightbulb.ToggleLight();
    }
}
