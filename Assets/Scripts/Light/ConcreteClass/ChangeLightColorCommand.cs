using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Concrete Command [ChangeLightCommand]
/// </summary>
/// <remarks>
/// <para> ConcreteCommand berfungsi untuk menginisiasi perintah spesifik untuk dijalankan pada receiver class. </para>
/// <para> ChangeLightCommand berfungsi untuk merubah warna cahaya pada lampu</para>
/// </remarks>
public class ChangeLightColorCommand : ICommand
{
    Lightbulb _lightbulb;
    Color _previousColor;

    public ChangeLightColorCommand(Lightbulb lightbulb)
    {
        _lightbulb = lightbulb;
        _previousColor = _lightbulb.GetComponent<Renderer>().material.color;
    }

    public void Execute()
    {
        _lightbulb.SetRandomLightColor();
    }

    public void Undo()
    {
        _lightbulb.SetLightColor(_previousColor);
    }
}
