using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Client 
/// </summary>
/// <remarks>
/// <para> Script client mengirim perintah untuk dijalankan oleh receiver. </para>
/// <para> Berdasarkan command-pattern, client hanya menjalankan abstract command dari concrete command </para>
/// </remarks>
public class UserInput : MonoBehaviour
{
    [SerializeField] private Lightbulb _lightbulb;
    LightController _lightController;

    private void Start()
    {
        _lightController = new LightController();
    }

    private void Update()
    {
        //Proses Command Pattern

        //Client menjalankan perintah
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // inisiasi concrete command untuk command spesifik yang ingin dijalankan, diinisiasi dari abstract command
            ICommand turnOnCommand = new TurnOnCommand(_lightbulb);
            //Invoke command dari Invoker
            _lightController.AddInteraction(turnOnCommand);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            ICommand switchRandomLightColor = new ChangeLightColorCommand(_lightbulb);
            _lightController.AddInteraction(switchRandomLightColor);
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            _lightController.UndoInteraction();
        }

    }
}
