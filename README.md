# Command Pattern

Command Pattern adalah sebuah design pattern dalam pengembangan perangkat lunak yang memungkinkan Anda untuk mengabstraksi request atau perintah sebagai objek terpisah. Dalam konteks Unity, Command Pattern bisa sangat berguna dalam mengelola input pemain, menyusun aksi yang dapat di-undo, atau bahkan dalam membangun sistem interaksi yang kompleks.

Secara umum, Command Pattern terdiri dari beberapa komponen:

![Command-Pattern components](ReadmeImage/command-pattern-concept.png)

- Command: Kelas yang mewakili perintah atau aksi tertentu. Biasanya memiliki metode Execute untuk menjalankan aksi dan bisa juga memiliki metode Undo untuk membatalkan aksi tersebut.

    - Abstract Command : Berfungsi sebagai kontrak metode Execute pada command. Tujuan penggunaan Abstract command adalah untuk mendeskripsikan secara explisit sebuah concrete command dengan meng-inherit abstract command dan memastikan setiap concrete command untuk memiliki method Execute.
    
    - Concrete Command : Berfungsi untuk menjalankan command spesifik yang ingin dijalankan

- Invoker: Kelas yang mengetahui bagaimana menghubungkan Command dengan Receiver. Ini bisa menjadi input handler atau bagian lain dari kode yang memicu perintah.

- Receiver: Kelas yang mengandung kode yang sebenarnya menjalankan aksi yang diminta oleh Command.

- Client: Kelas yang menginisiasi Command dan menghubungkannya dengan Invoker.

![Command-Pattern how it works](ReadmeImage/command-pattern_initial-pattern.png)

Pada dasarnya command pattern bekerja dengan class client yang mengirim trigger pada kelas receiver  untuk menjalankan aksi tertentu. Penggunaan command-pattern memungkinkan client dapat menjalankan satu trigger yang dapat menjalankan perintah pada objek/event yang berbeda. 

Sebagai contoh pada penggunaan command-pattern pada game turn-based. player dapat mentrigger event (bisa jadi banyak event berbeda dengan aksi dan logika yang berbeda). Meski antar satu event dan event lainnya berbeda event-event tersebut dapat di trigger dari satu class invoker yang sama.

![Command-Pattern pattern flow](ReadmeImage/command-pattern_pattern-flow.png)

Berdasarkan pattern flow diatas, client hanya perlu menginisiasi concrete command yang dibutuhkan yang kemudian disimpan pada invoker. kemudian invoker meng-eksekusi command tersebut sesuai dengan method pada receiver

Seperti yang dijelaskan pada deskripsi, keuntungan dari penggunaan command pattern adalah memungkinkan penggunaan fitur undo karena adanya kelas invoker yang memungkinkan untuk menyimpan data perubahan sebelumnya. 
