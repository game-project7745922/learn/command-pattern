using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstract Command
/// </summary>
/// <remarks>
/// Abstract command berfungsi sebagai kontrak pada concrete command untuk menginisiasi 
/// method yang sudah diinisiasi di abstract command.
/// </remarks>
public interface ICommand 
{
    void Execute();
    void Undo();
}
