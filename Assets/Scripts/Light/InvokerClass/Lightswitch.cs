using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Invoker
/// </summary>
/// <remarks>
/// Berfungsi untuk menginvoke method kelas receiver pada client melalui concrete command
/// </remarks>
public class Lightswitch 
{
    ICommand _IInteractable;

    public Lightswitch(ICommand interactable)
    {
        _IInteractable = interactable;
    }

    public void SwitchLight()
    {
        _IInteractable.Execute();
    }
}
