using System.Collections;
using System.Collections.Generic;
using System.Windows.Input;
using UnityEngine;

/// <summary>
/// Receiver
/// </summary>
public class Lightbulb : MonoBehaviour
{
    private bool _isTurnOn = false;

    Material _lightMaterial;
    string _emission = "_EMISSION";

    private void Awake()
    {
        _lightMaterial = GetComponent<Renderer>().material;
    }

    public void ToggleLight()
    {
        _isTurnOn = !_isTurnOn;

        if (_isTurnOn)
            _lightMaterial.EnableKeyword(_emission);

        else
            _lightMaterial.DisableKeyword(_emission);
    }

    public void SetRandomLightColor()
    {
        Color randomColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        Material material = _lightMaterial;
        material.color = randomColor;
        material.SetColor(_emission, randomColor);
    }

    public void SetLightColor(Color color)
    {
        Color prevColor = color;
        Material material = _lightMaterial;
        material.color = prevColor;
        material.SetColor(_emission, prevColor);
    }
}
