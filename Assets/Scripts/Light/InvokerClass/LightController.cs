using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Invoker
/// </summary>
/// <remarks>
/// Berfungsi untuk menginvoke method kelas receiver pada client melalui abstract command yang sudah diinisiasi oleh concrete command.
/// </remarks>
/// 
public class LightController
{
    Stack<ICommand> _interactionList;

    public LightController()
    {
        _interactionList = new Stack<ICommand>();
    }

    public void AddInteraction(ICommand interaction)
    {
        interaction.Execute();
        _interactionList.Push(interaction);
    }

    public void UndoInteraction()
    {
        if (_interactionList.Count > 0)
        {
            ICommand interactable = _interactionList.Pop();
            interactable.Undo();
        }
    }
}
